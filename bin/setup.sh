# Usage: source setup.sh [config]
#
# where 'config' is using the standard syntax: ARCH-OS-COMPILER-BUILD
# e.g. x86_64-slc6-gcc49-opt
#

############## configuration variables ################
# You can override this in your environment for testing other versions.

# The standard AFS location of LCG software
export LCG_RELEASE_BASE=${LCG_RELEASE_BASE:=/afs/cern.ch/sw/lcg/releases}
LCG_BASE=$(dirname ${LCG_RELEASE_BASE})

# The location of LCG contrib and external
EXTERNAL_BASE=${EXTERNAL_BASE:=${LCG_BASE}/external}
CONTRIB_BASE=${CONTRIB_BASE:=${LCG_BASE}/contrib}

# The location and version of CMake to use
CMAKE_BASE=${CMAKE_BASE:=${CONTRIB_BASE}/CMake}
CMAKE_VERSION=${CMAKE_VERSION:=3.6.0}
CMAKE_PATH=${CMAKE_PATH:=${CMAKE_BASE}/${CMAKE_VERSION}/Linux-x86_64/bin}

# For TDAQ projects
CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH:=/afs/cern.ch/atlas/project/tdaq/cmake/projects}

# export JAVA_HOME=${JAVA_HOME:=${EXTERNAL_BASE}/Java/JDK/1.8.0/amd64}

############## end of configuration variables ################

# Setup path for CMAKE
export PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]-$0})):${CMAKE_PATH}:${PATH}

# determine OS
case "${1}" in
    *-slc6-*)
        CMAKE_ARCH=x86_64-slc6
        ;;
    *-centos7-*)
        CMAKE_ARCH=x86_64-centos7
        ;;
    *)
        CMAKE_ARCH=x86_64-slc6
        ;;
esac

# Choose compiler, this has to reflect what you choose in the tag to
# cmake_config later
case "${1}" in
    *-gcc*)
        gcc_requested=$(echo "${1}" | cut -d- -f3 | tr -d 'gcc' | sed 's;\(.\);\1.;g' | sed 's;\.$;;')
        source ${CONTRIB_BASE}/gcc/${gcc_requested}/${CMAKE_ARCH}/setup.sh
        unset gcc_requested
        ;;
    *-clang*)
        clang_requested=$(echo "${1}" | cut -d- -f3 | tr -d 'clang' | sed 's;\(.\);\1.;g' | sed 's;\.$;;')
        source ${EXTERNAL_BASE}/llvm/${clang_requested}/${CMAKE_ARCH}/setup.sh
        unset clang_requeste
        ;;
    *)
        #default
        source ${CONTRIB_BASE}/gcc/6.2/${CMAKE_ARCH}/setup.sh
        ;;
esac

export gcc_config_version
    
# Setup CMAKE_PREFIX_PATH to find LCG
export CMAKE_PREFIX_PATH=${CMAKE_PROJECT_PATH}:$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]-$0})))/cmake

unset CMAKE_BASE CMAKE_VERSION CMAKE_PATH GCC_BASE CMAKE_ARCH LCG_BASE EXTERNAL_BASE CONTRIB_BASE
