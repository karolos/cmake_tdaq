#
# Find and include the TDAQ specific functions
#

get_filename_component(_dir ${CMAKE_CURRENT_LIST_DIR} ABSOLUTE)
list(APPEND CMAKE_MODULE_PATH ${_dir}/modules)
unset(_dir)

message(STATUS "Found TDAQ: ${CMAKE_CURRENT_LIST_FILE}")

# Check for build configuration in a variety for places:
# -DBINARY_TAG=... - from the command line
# $ENV{BINARY_TAG} - like set by cmake_config
# $ENV{CMTCONFIG}     - fall back, if cm_setup for a given release/config was deon

if(NOT DEFINED BINARY_TAG)
  set(BINARY_TAG $ENV{BINARY_TAG})
  if("${BINARY_TAG}" STREQUAL "") 
    set(BINARY_TAG $ENV{CMTCONFIG})
    if("${BINARY_TAG}" STREQUAL "")
      message(FATAL_ERROR "Please set either the BINARY_TAG or CMTCONFIG variable to a valid configuration")
    else()
      message(STATUS "Choosing build tag based on CMTCONFIG: ${BINARY_TAG}")
    endif()
  endif()
endif()

set(BINARY_TAG ${BINARY_TAG} CACHE STRING "Platform ID" FORCE)
set(BUILDNAME ${BINARY_TAG} CACHE STRING "Build name for CDash")

if(${BINARY_TAG} MATCHES ".*-opt")
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING "Build type" FORCE)
else()
  set(CMAKE_BUILD_TYPE "Debug" CACHE STRING "Build type" FORCE)
endif()

include(TDAQ)
