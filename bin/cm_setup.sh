#!/bin/echo "Please source this script, do not execute it"
# You can still debug it with 'sh -x cm_setup.sh ...'
#
# More general user setup script a la cmtsetup.sh
# It can setup both build and/or run-time environments, or just run-time environment.
# 
# If no configuration is given, a default one based on the OS version is chosen.
#
# Usage:
#
# source /path/to/cm_setup.sh [--runtime|-r] <project>-<version> [ configuration ]
#
# Script:
#
# /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh
# 
# The script assumes a number of locations where to search:
#
# /sw/atlas/<project>/<version>/installed/setup.sh
# /afs/cern.ch/atlas/project/tdaq/inst/<project>/<version>/installed/setup.sh
# /afs/cern.ch/atlas/project/tdaq/cmake/projects/<project>/<version>/installed/setup.sh
# 
# If this location is not correct, it should be specified in TDAQ_RELEASE_BASE which will
# override all default paths.
#
# Examples:
#
# Setup only run-time environment, this is equivalent to sourcing the <project>/<version>/installed/setup.sh [configuration]# No build commands like cmake_config will be available.
# 
#     source /path/to/cm_setup.sh -r tdaq-06-02-00 x86_64-centos7-gcc49-opt
#     source /path/to/cm_setup.sh -r tdaq-06-02-00 
# 
# If the -r option is not given, both the build followed by the run-time setup is done.
# 

if [ "${1}" = "--help" -o "${1}" = "-h" ]; then
    echo "usage: source ${BASH_SOURCE[0]-$0} [-r|--runtime] <project> [ <configuration> ]"
    echo "      <project> should be a versioned project name like tdaq-06-02-00"
    echo "      <configuration> should be a valid build configuration, like x86_64-slc6-gcc49-opt"
    echo "      The default is to setup the build environment, followed by the run-time environment"
    echo "      If no configuration is given a default configuration is chosen by the project itself"
    return 0
fi

_cmake_tdaq=$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]-$0})))

if [ ! -z "${TDAQ_RELEASE_BASE}" ]; then
    _candidates=${TDAQ_RELEASE_BASE}
else
    for _cand in `echo /sw/atlas /afs/cern.ch/atlas/project/tdaq/inst /afs/cern.ch/atlas/project/tdaq/cmake/projects`
    do
        if [ -d ${_cand} ]; then
            _candidates="${_candidates} ${_cand}"
        fi
    done
fi

_runtime_only=0
if [ "$1" = "-r" -o "$1" = "--runtime" ]; then
    _runtime_only=1
    shift
fi

if [ $# -eq 0 ]; then
  _project=prod
else
  _project=$1
  shift
fi

case ${_project} in
    tdaq-common-*)
        _project_base=tdaq-common
        ;;
    dqm-common-*)
        _project_base=dqm-common
        ;;
    *)
        _project_base=tdaq
        ;;
esac

# Note: if not configuration is given but the project exists, we leave it to the
# project setup.sh script to choose a default version. This is better than hard-coding
# defaults for different versions in here.

_config=${1:-${CMTCONFIG}}

for _cand in `echo ${_candidates}`; do
    if [ -f ${_cand}/${_project_base}/${_project}/installed/setup.sh ]; then
        export TDAQ_RELEASE_BASE=${_cand}
        break
    fi
done

if [ ! -f ${TDAQ_RELEASE_BASE}/${_project_base}/${_project}/installed/setup.sh ]; then
    echo "Cannot find possible release candidate in: ${_candidates}"
    return 2
fi

if [ -z "${CMAKE_PROJECT_PATH}" ]; then
  export CMAKE_PROJECT_PATH=${TDAQ_RELEASE_BASE}
fi

if [ ! -z "${_config}" ]; then
  export CMTCONFIG=${_config}
fi

source ${TDAQ_RELEASE_BASE}/${_project_base}/${_project}/installed/setup.sh ${_config}

if [ ${_runtime_only} -eq 0 ];then
    case "${TDAQC_INST_PATH}" in
       */tdaq-common-02-00-00/*)
          export CMAKE_VERSION=3.6.0
          ;;
       *)
          export CMAKE_VERSION=3.6.0
          ;;
    esac
    if [ -d ${TDAQC_INST_PATH}/share/cmake_tdaq ]; then
        source ${TDAQC_INST_PATH}/share/cmake_tdaq/bin/setup.sh ${_config}
    else
        source ${_cmake_tdaq}/bin/setup.sh ${_config}
    fi
    export PATH=/afs/cern.ch/sw/lcg/contrib/gdb/7.8/x86_64-slc6-gcc48-opt/bin:${PATH}
fi

case `uname -n` in
    *-tbed-*)
        echo "Setting up environment specific to test-bed"
        TDAQ_DB_PATH="/tbed/oks/${_project}":$TDAQ_DB_PATH
        ;;
    pc-atlas-*|vm-atlas-*|pc-tdq-*)
        echo "Setting up environment specific to Point 1"
        TDAQ_DB_PATH="/atlas/oks/${_project}":$TDAQ_DB_PATH
        ;;
esac

unset _cmake_tdaq _runtime_only _project _project_base_config _os _cand _candidates
