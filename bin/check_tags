#!/bin/bash
#
# check_tags [ -d ] project release
#
TAGCOLLECTOR=${TAGCOLLECTOR:=/afs/cern.ch/atlas/project/tdaq/cmt/adm/packages}
CHECKOUT_AREA=${CHECKOUT_AREA:=/afs/cern.ch/atlas/project/tdaq/cmake/projects}

diff=0
if [ "${1}" == "-d" ]; then
    diff=1
    shift
fi

if [ $# -lt 2 ]; then
   echo "Usage: $0 project release"
   echo "  e.g.  $0 tdaq-common 2.0.0"
   exit 1
fi

if [ ! -d ${TAGCOLLECTOR}/${1}/${2} ]; then
    echo "No such release: ${1}/${2}"
    exit 1
fi

if [ ! -d ${CHECKOUT_AREA}/${1}/${2} ]; then
   echo "No checked out release at ${CHECKOUT_AREA}/${1}/${2}"
   exit 2
fi

cd ${CHECKOUT_AREA}/${1}/${2} || exit 2

echo "Project: ${1}/${2}: $(git describe --always --tags)"
git status -s

printf "package\t\tCollector\t\tCheckout\n"
echo   "=============================="
for pkg in ${TAGCOLLECTOR}/${1}/${2}/*
do
    name=$(basename $pkg)
    tag=$(tail -1 $pkg)
    co=`(cd ${name} && git describe --always --tags)`
    if [ $diff -eq 1 ]; then
        if [ "${tag}" != "${co}" ]; then
            # this handles the case where two tags point to the same commit
            # and avoids unecessary warnings
            tref=$(git show-ref -s ${tag})
            cref=$(git show-ref -s ${co})
            if [ "${tref}" != "${cref}" ]; then
                printf "${name}\t${tag}\t${co}"
            fi
            (cd ${name} && git status -s)
        fi
    else
        printf "${name}\t${tag}\t${co}\n"        
        (cd ${name} && git status -s)
    fi
done
