#!/usr/bin/env python

import sys
import re
import string
from xml.dom import minidom
from os import popen

doc = minidom.parse(sys.argv[1])
source = sys.argv[2]
binary = sys.argv[3]

def compare(a,b):
   if a['Error'] == b['Error']: return cmp(b['Warning'], a['Warning'])
   return cmp(b['Error'], a['Error'])

failures = {}

for c in doc.getElementsByTagName('Failure'):
    t = c.getAttribute('type')
    if not t in ['Warning', 'Error']: continue
    wd = c.getElementsByTagName('WorkingDirectory')[0].firstChild.nodeValue
    if wd == binary or wd == source: continue
    try:
       pkg = wd.replace(binary,'').replace(source,'').split('/')[1]
    except:
       continue
    if not failures.has_key(pkg):
       failures[pkg] = { 'Name': pkg, 'Warning': 0, 'Error': 0 , 'Files': {}}
    failures[pkg][t] += 1
    source_file = c.getElementsByTagName('SourceFile')
    if source_file:
      source_file = source_file[0].firstChild.nodeValue
      if not failures[pkg]['Files'].has_key(source_file):
        failures[pkg]['Files'][source_file] = { 'Warning': 0, 'Error': 0 } 
      failures[pkg]['Files'][source_file][t] += 1

fail_list = [ f for f in failures.values() ]
fail_list.sort(cmp=compare)

print '''<html>
<head>
  <style>
    .Error {
       background-color: #F1CE68;
    }
  </style>
  <title>Errors and warnings per package</title>
  <script type="text/javascript" src="../../javascript/jquery-1.6.2.js"></script> 
  <script type="text/javascript" src="../../javascript/jquery.tablesorter.js"></script> 
  <script type="text/javascript">
     $(document).ready(function() 
    { 
        $("#theTable").tablesorter(); 
    } 
); 
  </script>
</head>
<body>
<h1>Error and Warning Summary per Package</h1>
'''

print '<table id="theTable" class="tablesorter" frame="border" rules="all" ><thead><tr><th>Package</th><th>Errors</th><th>Warnings</th><th>Commit</th><th>Files</th></tr></thead><tbody>'

error_packages   = [ '<a href="#%s">%s</a>' % (out['Name'], out['Name']) for out in fail_list if out['Error'] > 0]
warning_packages = [ '<a href="#%s">%s</a>' % (out['Name'], out['Name']) for out in fail_list if out['Error'] == 0]

error_packages.sort()
warning_packages.sort()

print '<p><b>Errors in:</b><br>',string.join(error_packages,', ')
print
print '<p><b>Warnings in:</b><br>', string.join(warning_packages,', ')

pattern = re.compile('commit (.*)\n')
email   = re.compile('Author: (.*)<(.*)>')

for out in fail_list:
   log = popen('cd %s; git log --format=medium -1' % (source + '/' + out['Name']),'r')
   log_txt = log.read()
   log_txt = re.sub(pattern, 'commit <a href="https://gitlab.cern.ch/atlas-tdaq-software/%s/commit/\\1">\\1</a>\n' % out['Name'], log_txt)
   log_txt = re.sub(email, 'Author: \\1<a href="mailto:\\2">&lt;\\2&gt;<a>', log_txt)
   
   print '<tr class="%s">' % (out['Error'] > 0 and 'Error' or 'Warning'),'<th><a name="%s" href="https://gitlab.cern.ch/atlas-tdaq-software/%s">' % (out['Name'], out['Name']),out['Name'],"</a></th><td>",out['Error'],'</td><td>',out['Warning'],'</td><td><pre>' + log_txt,'</pre></td><td>'
   for s in out['Files']:
     print '<code>'+ s + '</code>' + ':',out['Files'][s]['Error'],'error(s)',out['Files'][s]['Warning'],'warning(s)<br>'
   print "</tr>"
   log.close()
print "</tbody></body></html>"

